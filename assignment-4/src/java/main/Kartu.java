import javax.swing.JButton;
import javax.swing.ImageIcon;

public class Kartu{
    private ImageIcon gambarDepan;
    private ImageIcon gambarBelakang;
    private int id;
    private boolean cocok=false;
    private final JButton tombol = new JButton();

    public Kartu(int id,ImageIcon a,ImageIcon b){
        this.gambarDepan=a;
        this.gambarBelakang=b;
        this.id=id;
        this.cocok=cocok;
        this.tombol.setIcon(a);
    }

    public void ditekan(){
      tombol.setIcon(gambarBelakang);
    }
    public void dilepas(){
        tombol.setIcon(gambarDepan);
    }
    public ImageIcon getGambarDepan() {
        return gambarDepan;
    }

    public void setGambarDepan(ImageIcon gambarDepan) {
        this.gambarDepan = gambarDepan;
    }

    public ImageIcon getGambarBelakang() {
        return gambarBelakang;
    }

    public void setGambarBelakang(ImageIcon gambarBelakang) {
        this.gambarBelakang = gambarBelakang;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isCocok() {
        return cocok;
    }

    public void setCocok(boolean cocok) {
        this.cocok = cocok;
    }
    public JButton getTombol() {
        return tombol;
    }
}