import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.*;

public class Papan {
    private List<Kartu> lista;
    private JPanel papan;
    private JPanel papanexit;
    private Kartu kartupertama;
    private Kartu kartukedua;
    private JFrame frame;
    private int coba = 0;
    private JLabel word2;
    private Timer waktu;
    private Timer mulaiingat;
    private Timer melihat;

public Papan(){
    List<Kartu> listKartu = new ArrayList<>();
    List<Integer> simpan = new ArrayList<>();

    for (int i = 0; i < 18; i++) {
       simpan.add(i);
        simpan.add(i);
    }
    Collections.shuffle(simpan);
    for (int k : simpan) {
        String[] gambar = {"ariel", "aurora", "jasmine", "cinderella", "rapunzel", "mulan", "belle", "snow white", "pochahontas", "pacarnya belle", "pacarnya ariel", "pacarnya cinderella", "pacarnya jasmine", "pacarnya pocahontas", "pacarnya snow white", "pacarnya aurora", "pacarnya mulan", "pacarnya rapunzel"};
        String path = null;
        try {
            path = System.getProperty("user.dir")+ "\\disney\\";
        } catch (Exception e) {
            System.out.println("Images path is not valid");
        }
        Kartu a = new Kartu(k, resizeIcon(new ImageIcon(path + "logo.jpg")), resizeIcon(new ImageIcon(path + gambar[k]+".jpg" )));
        a.getTombol().addActionListener(e -> PencetKartu(a));
        listKartu.add(a);
    }
    this.lista=listKartu;
    waktu = new Timer(750, e -> CekKartu());
    waktu.setRepeats(false);
    Utama();
    }


    public void Utama(){
        frame = new JFrame("Match the Disney Princess and Prince!");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        papan = new JPanel();
        papan.setLayout(new GridLayout(6,6));
        for(Kartu a : lista) {
            papan.add(a.getTombol());
        }

        papanexit = new JPanel();
        papanexit.setLayout(new GridLayout(2,2));
        JButton exit = new JButton("EXIT");
        exit.addActionListener(actionPerformed->System.exit(0));
        JButton reset = new JButton("RESET");
        reset.addActionListener(actionPerformed->Reset());
        papanexit.add(reset);
        papanexit.add(exit);
        word2 = new JLabel("Number of Tries: " + coba);
        word2.setHorizontalAlignment(SwingConstants.CENTER);
        papanexit.add(word2);



        frame.add(papan,BorderLayout.CENTER);
        frame.add(papanexit,BorderLayout.SOUTH);
        frame.setPreferredSize(new Dimension(600, 600));
        frame.setLocation(500, 100);
        frame.pack();
        frame.setVisible(true);
        mulaiingat = new Timer(2000, actionPerformed -> Buka());
        mulaiingat.start();
        mulaiingat.setRepeats(false);
    }
    public void Reset() {
        frame.remove(papan);
        Collections.shuffle(lista);
        JPanel papan = new JPanel(new GridLayout(6, 6));
        for (Kartu a : lista) {
            a.setCocok(false);
            a.dilepas();
            a.getTombol().setEnabled(true);
            papan.add(a.getTombol());
        }
        frame.add(papan,BorderLayout.CENTER);
        coba=0;
        word2.setText("Number of Tries: " + coba);
        Buka();
    }
    public void Tutup() {
            for (Kartu b : lista) {
                b.dilepas();
            }
            papan.updateUI();
        }

        public boolean Menang() {
            for (Kartu c : this.lista) {
                if (!c.isCocok()) {
                    return false;
                }
            }
            return true;
        }
    public void Buka() {
        for (Kartu d : lista) {
            d.ditekan();
        }
        papan.updateUI();
        melihat = new Timer(1000, actionPerformed -> Tutup());
        melihat.setRepeats(false);
        melihat.start();
    }

    public void TombolSesuai() {
        kartupertama.getTombol().setEnabled(false);
        kartukedua.getTombol().setEnabled(false);
        kartupertama.setCocok(true);
        kartukedua.setCocok(true);
    }

    public void CekKartu() {
        if (kartupertama.getId() == kartukedua.getId()) {
            kartupertama.getTombol().setEnabled(false);
            kartukedua.getTombol().setEnabled(false);
            kartupertama.setCocok(true);
            kartukedua.setCocok(true);
            if (this.Menang()) {
                JOptionPane.showMessageDialog(frame, "You win!\nNumber of "
                        + "tries: " + coba);
                System.exit(0);
            }
        } else {
            kartupertama.dilepas();
            kartukedua.dilepas();
            coba++;
            word2.setText("Number of Tries: " + coba);
        }
        kartupertama= null; //reset c1 and c2
        kartukedua = null;
    }

    public void PencetKartu(Kartu dipencet) {
        if (kartupertama == null && kartukedua == null) {
            kartupertama = dipencet;
            kartupertama.ditekan();
        }

        if (kartupertama != null && kartupertama != dipencet && kartukedua == null) {
            kartukedua = dipencet;
            kartukedua.ditekan();
            waktu.start();
        }
    }


    public static ImageIcon resizeIcon(ImageIcon icon) {
        Image img = icon.getImage();
        Image resizedImage = img.getScaledInstance(100,90, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedImage);
    }

}