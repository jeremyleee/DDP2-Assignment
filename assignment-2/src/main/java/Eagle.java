
public class Eagle extends Animals{
    public Eagle(String name, int length) {
        super(name, length);
        Cagetype(false);
    }
    public void dosomething(int number) {
        if (number == 1) {
            this.Fly();
        } else {
            System.out.println("You do nothing...");
            System.out.println("Back to the office!");
            System.out.println();
        }
    }
    public void Fly(){
        System.out.println( this.getName() + " makes a voice: kwaakk...\n" + "You hurt!");
        System.out.println("Back to the office!");
        System.out.println();
    }
}
