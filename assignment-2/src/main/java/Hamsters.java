
public class Hamsters extends Animals  {
    public Hamsters(String name,int length){
        super(name,length);
        this.name=name;
        this.length=length;
        Cagetype(true);
    }
    public void dosomething(int number){
        if(number==1){
            this.Gnaw();
        }
        else if(number==2){
            this.Run();
        }
        else{
            System.out.println("You do nothing...");
            System.out.println("Back to the office!");
            System.out.println();
        }}
    public void Gnaw(){
        System.out.println(this.getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
        System.out.println("Back to the office!");
        System.out.println();
    }
    public void Run(){
        System.out.println(this.getName() + " makes a voice: trrr…. trrr...");
        System.out.println("Back to the office!");
        System.out.println();
    }
}
