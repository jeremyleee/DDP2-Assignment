
import java.util.Scanner;
public class Parrots extends Animals {
    public Parrots(String name,int length){
        super(name,length);
        this.name=name;
        this.length=length;
        Cagetype(true);
    }
    public void dosomething(int number){
        if(number==1){
            this.Fly();
        }
        else if(number==2){
            this.Talk();
        }
        else{
            System.out.println("Greeny says: HM?");
            System.out.println("Back to the office!");
        }}
    public void Fly(){
        System.out.println("Parrot" + this.getName() + " flies! ");
        System.out.println(this.getName()+" make a voice: FLYYYY…..");
        System.out.println("Back to the office!");
        System.out.println();
    }
    public void Talk(){
        Scanner in=new Scanner(System.in);
        System.out.print("You say: ");
        String a=in.nextLine();
        System.out.println(this.getName()+ " say: "+a.toUpperCase());
        System.out.println("Back to the office!");
        System.out.println();
    }
}
