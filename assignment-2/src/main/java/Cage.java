
import java.util.ArrayList;
public class Cage{
    private ArrayList<Animals> list1;
    private Animals list2[][];
    private int size;

   public Cage(ArrayList<Animals> list1) {
        this.list1 = list1;}
    public void arrange(){
        int indeks = 0;

        if (list1.size() > ((list1.size() / 3) * 3)) {
            size =list1.size() / 3;
            indeks = 3 - (list1.size() - size * 3);
        } else {
            size =list1.size() / 3 - 1;
        }
        list2= new Animals[3][size + 1];
        int indeks2 = 0;

        if (size == 0) {
            for (int a = 0; a <list1.size(); a++) {list2[a][0] =list1.get(a);
            }
        } else {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < size + 1; j++) {
                    if (j == size && i < indeks) {
                        break;
                    } else {list2[i][j] =list1.get(indeks2);
                        indeks2++;
                    }
                }
            }
        }

        System.out.println("location: " +list2[0][0].getTempatkandang());
        printAnimals(list2, size);
        System.out.println("\n");
    }
    //Method untuk mengarrange cage nya
    public void afterArrange() {
        Animals listAfterArrange[][];
        listAfterArrange = new Animals[3][size + 1];
        int temp1 = 1;
        for (int i = 2; i >= 0; i--) {
            int temp2 = size;
            if (temp1 == -1) {
                temp1 = 2;
            }
            for (int j = 0; j < size + 1; j++) {
                listAfterArrange[i][j] = list2[temp1][temp2];
                temp2--;
            }
            temp1--;
        }
        System.out.println("After rearrangement...");
        printAnimals(listAfterArrange, size);
        System.out.println();
    }

    //Method untuk ngeprint setiap cagenya (baik sebelum maupun sesudah
    private void printAnimals(Animals listAnimals[][], int size) {
        for (int k = 2; k >= 0; k--) {
            System.out.print("Level " + (k + 1) + ": ");
            for (int l = 0; l < size + 1; l++) {
                if (listAnimals[k][l] != null) {
                    System.out.print(listAnimals[k][l].getName() + "(" +
                            listAnimals[k][l].getLength() + " - " +
                            listAnimals[k][l].getKodekandang() + "), ");
                }
            }
            System.out.print("\n");
        }
    }
}




