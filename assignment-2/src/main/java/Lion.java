
public class Lion extends Animals{
    public Lion(String name, int length) {
        super(name, length);
        Cagetype(false);
    }
    public void dosomething(int number) {
        if (number == 1) {
            this.Hunt();
        } else if (number == 2) {
            this.Brush();
        }
        else if(number==3){
            this.Disturb();
        }
        else {
            System.out.println("You do nothing...");
            System.out.println("Back to the office!");
            System.out.println();
        }
    }

    public void Hunt(){
        System.out.println("Lion is hunting..");
        System.out.println(this.getName()+" make a voice: Err....");
        System.out.println("Back to the office!");
        System.out.println();
    }
    public void Brush(){
        System.out.println("Clean the lion’s mane..");
        System.out.println(this.getName()+" make a voice: Haumm...");
        System.out.println("Back to the office!");
        System.out.println();
    }
    public void Disturb(){ System.out.println(this.getName()+" make a voice: HAUMM...");
        System.out.println("Back to the office!");
        System.out.println();
    }
}
