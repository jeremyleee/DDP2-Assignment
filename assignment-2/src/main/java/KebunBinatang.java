
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
public class KebunBinatang {
    private static ArrayList<Animals> cats = new ArrayList<>();
    private static ArrayList<Animals> hamsters = new ArrayList<>();
    private static ArrayList<Animals> parrots = new ArrayList<>();
    private static ArrayList<Animals> eagles = new ArrayList<>();
    private static ArrayList<Animals> lions = new ArrayList<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int jumlahbinatang[] = new int[5];
        String[] listType = {"cat" , "lion" , "parrot" , "eagle" , "hamster"};
        System.out.println("Welcome to Javari Park!\n" +
                "Input the number of animals");
        KebunBinatang zoo = null;
        for (int i = 0; i < 5; i++) {
            System.out.print(listType[i] + ": ");
            int numanimal = in.nextInt();
            jumlahbinatang[i]=numanimal;
            if (numanimal != 0) {
                zoo = new KebunBinatang();
                zoo.informations(i);
            }
        }
        System.out.println("Animals have been successfully recorded!");

        zoo.printArrange();

        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat:"+jumlahbinatang[0]);
        System.out.println("lion:"+jumlahbinatang[1]);
        System.out.println("parrot:"+jumlahbinatang[2]);
        System.out.println("eagle:"+jumlahbinatang[3]);
        System.out.println("hamster:"+jumlahbinatang[4]);
        System.out.println();
        System.out.println();
        System.out.println("=============================================");
        zoo.visit();

    }
    //public void

    public void informations(int i) {
        Scanner in = new Scanner(System.in);
        String[] listType = {"cat" , "lion" , "parrot" , "eagle" , "hamster"};
        if (listType[i].equals("cat")) {
            System.out.print("Provide the information of cat(s): ");
            String infocat = in.next();
            String[] kandangCat = infocat.split(",");
            for (int c = 0; c < kandangCat.length; c++) {
                String[] acat = kandangCat[c].split("[|]");
                Cat kucing = new Cat(acat[0], Integer.parseInt(acat[1]));
                cats.add(kucing);
            }
        }

        else if (listType[i].equals("hamster")) {
                System.out.print("Provide the information of hamster(s): ");
                String infohamster = in.next();
                String[] kandangHamster = infohamster.split(",");
                for (int c = 0; c < kandangHamster.length; c++) {
                    String[] ahamster = kandangHamster[c].split("[|]");
                    Hamsters hamster = new Hamsters(ahamster[0], Integer.parseInt(ahamster[1]));
                    hamsters.add(hamster);
                }
            }
      else if (listType[i].equals("parrot")) {
                System.out.print("Provide the information of parrot(s): ");
                String infoparrot = in.next();
                String[] kandangParrot = infoparrot.split(",");
                for (int c = 0; c < kandangParrot.length; c++) {
                    String[] aparrot = kandangParrot[c].split("[|]");
                    Parrots parrot = new Parrots(aparrot[0], Integer.parseInt(aparrot[1]));
                    parrots.add(parrot);
                }
            }
        if (listType[i].equals("eagle")) {
                System.out.print("Provide the information of eagle(s): ");
                String infoeagle = in.next();
                String[] kandangEagle = infoeagle.split(",");
                for (int c = 0; c < kandangEagle.length; c++) {
                    String[] aeagle = kandangEagle[c].split("[|]");
                    Eagle eag = new Eagle(aeagle[0], Integer.parseInt(aeagle[1]));
                    eagles.add(eag);
                }
            }
        if (listType[i].equals("lion")) {
            System.out.print("Provide the information of lion(s): ");
            String infolion = in.next();
            String[] kandangLion = infolion.split(",");
            for (int c = 0; c < kandangLion.length; c++) {
                String[] alion = kandangLion[c].split("[|]");
                Lion singa = new Lion(alion[0], Integer.parseInt(alion[1]));
                lions.add(singa);
            }
        }
    }

    public void visit() {
        while (true) {
            Scanner ina = new Scanner(System.in);
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            String angka = ina.nextLine();

            if (angka.equals("1")) {
                System.out.print("Mention the name of cat you want to visit: ");
                String nama = ina.nextLine();
                for (Animals a : cats) {
                    if (nama.equals(a.getName())) {
                        System.out.println("You are visiting " + nama + "(cat) now, what would you like to do?");
                        System.out.println("1: Brush the fur 2: Cuddle");
                        int pilihanaksi = ina.nextInt();
                        a.dosomething(pilihanaksi);
                    } else {
                        System.out.println("There is no cat with that name! Back to the office! ");
                        System.out.println();

                    }
                }

            } else if (angka.equals("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String namae = ina.nextLine();
                for (Animals a : eagles) {
                    if (namae.equals(a.getName())) {
                        System.out.println("You are visiting " + namae + "(eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly");
                        int pilihanaksi = ina.nextInt();
                        a.dosomething(pilihanaksi);
                    } else {
                        System.out.println("There is no eagle with that name! Back to the office!");
                        System.out.println();
                    }
                }

            } else if (angka.equals("3")) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String namah = ina.next();
                for (Animals a : hamsters) {
                    if (namah.equals(a.getName())) {
                        System.out.println("You are visiting " + namah + "(hamster) now, what would you like to do?");
                        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                        int pilihanaksi = ina.nextInt();
                        a.dosomething(pilihanaksi);
                    } else {
                        System.out.println("There is no hamster with that name! Back to the office!");
                        System.out.println();
                    }
                }

            } else if (angka.equals("4")) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String namap = ina.next();
                for (Animals a : parrots) {
                    if (namap.equals(a.getName())) {
                        System.out.println("You are visiting " + namap + "(parrot) now, what would you like to do?");
                        System.out.println("1: Order to fly 2: Do conversation");
                        int pilihanaksi = ina.nextInt();
                        a.dosomething(pilihanaksi);
                    } else {
                        System.out.println("There is parrot with that name! Back to the office!");
                        System.out.println();
                    }
                }
            } else if (angka.equals("5")) {
                System.out.print("Mention the name of lion you want to visit: ");
                String namal = ina.next();
                for (Animals a : lions) {
                    if (namal.equals(a.getName())) {
                        System.out.println("You are visiting " + namal + "(lion) now, what would you like to do?");
                        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                        int pilihanaksi = ina.nextInt();
                        a.dosomething(pilihanaksi);
                    } else {
                        System.out.println("There is no lion with that name! Back to the office!");
                        System.out.println();
                    }
                }
            } else {
                break;
            }
        }
        }
    private void printArrange() {
        System.out.println("\n");
        System.out.println("=============================================\n" +
                "Cage arrangement:");
        if (cats.size() != 0) {
            Cage catCage = new Cage(cats);
            catCage.arrange();
            catCage.afterArrange();
        }
        if (lions.size() != 0) {
            Cage lionCage = new Cage(lions);
            lionCage.arrange();
            lionCage.afterArrange();
        }
        if (eagles.size() != 0) {
            Cage eagleCage = new Cage(eagles);
            eagleCage.arrange();
            eagleCage.afterArrange();
        }
        if (parrots.size() != 0) {
            Cage parrotCage = new Cage(parrots);
            parrotCage.arrange();
            parrotCage.afterArrange();
        }
        if (hamsters.size() != 0) {
            Cage hamsterCage = new Cage(hamsters);
            hamsterCage.arrange();
            hamsterCage.afterArrange();
        }
    }

}



