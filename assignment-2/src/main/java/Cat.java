
import java.util.Random;
public class Cat extends Animals  {

    public Cat(String name,int length){
        super(name,length);
        this.name=name;
        this.length=length;
        Cagetype(true);
    }
    public void dosomething(int number){
        if(number==1){
            this.Cuddle();
        }
        else if(number==2){
            this.Brush();
        }
        else{
            System.out.println("You do nothing...");
            System.out.println("Back to the office!");
        }
    }
    public void Cuddle(){
        String[] sounds=new String[]{"Miaaaw..","Purrr..", "Mwaw!","Mraaawr!"};
        Random rand=new Random();
        int n=rand.nextInt(5);

        System.out.println(this.getName()+" makes a voice: "+sounds[n]);
        System.out.println("Back to the office!");
        System.out.println();
    }
    public void Brush(){
        System.out.println("Time to clean "+this.getName()+"'s fur");
        System.out.println(this.getName()+" makes a voice: Nyaaan….");
        System.out.println("Back to the office!");
        System.out.println();
    }
}
