public class TrainCar {
    public static final double EMPTY_WEIGHT=20;

        WildCat cat;
        TrainCar next;
    public TrainCar(WildCat cat) {
        this.cat = cat;
        this.next = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }
        public double computeTotalWeight () {
        if (this.next == null) {
            return (EMPTY_WEIGHT + this.cat.weight);
        } else {
            return (EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight());
        }
    }
        public double computeTotalMassIndex () {
        if (this.next == null) {
            return (this.cat.computeMassIndex());
        } else {
            return (this.cat.computeMassIndex() + this.next.computeTotalMassIndex());
        }
    }
        public void printCar () {
        if (this.next == null) {
            System.out.printf("(%s)",this.cat.name);
        } else {
            System.out.printf ("(%s)--", this.cat.name);
            this.next.printCar();
        }
    }

}
