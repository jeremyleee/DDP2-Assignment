import java.util.Scanner;
public class WildCat {
    String name;
    double weight;
    double length;

    public  WildCat(String name , double weight, double length) {
        this.name=name;
        this.weight=weight;
        this.length=length;
    }


    public double computeMassIndex(){
        double BMI= weight/(length/100*length/100);
        return (BMI);

    }
}
