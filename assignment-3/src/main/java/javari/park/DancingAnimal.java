import javari.animal.Animal;
import javari.park.SelectedAttraction;
import java.util.ArrayList;
import java.util.List;
public class CircleOfFire implements SelectedAttraction{
    private String name = "Dancing Animals";
    private List<Animal> listpenampil = new ArrayList<>();
    private String type;

    public CircleOfFire(String type){
        this.type = type;
    }
    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }

    public List<Animal> getListpenampil() {
        return listpenampil;
    }

    @Override
    public boolean addPerformer(Animal performer){
        if(performer.isShowable()){
            listPerformers.add(performer);
            return true;
        }
        else{
            return false;
        }
    }
}