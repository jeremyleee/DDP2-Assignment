package javari.animal.Mammals;
import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;
public class Mammals extends Animal{
   private boolean isPregnant;
    public Mammals(int id, String name, String gender, double length,
                  double weight,String pregnant, Condition condition) {
        super(id, name, gender, length, weight);
                if(pregnant.equals("pregnant")){
                    this.isPregnant==true;
                }
                else{
                    this.isPregnant==false;
                }
    }
    public boolean isPregnant() {
        return isPregnant;
    }
    public boolean spesificCondition(){
        return !(isPregnant);
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }
}