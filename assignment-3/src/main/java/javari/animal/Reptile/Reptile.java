public class Reptile extends Animal {
    private boolean isTame;

    public Reptile(int id, String name, String gender, double length,
                   double weight,String tame, Condition condition) {
        super(id, name, gender, length, weight);
        if(tame.equals("tame")){
            this.isTame==true;

        }
        else{
            this.isTame==false;
        }
        public boolean spesificCondition(){
            return (isTame);
        }
        public boolean isTame() {
            return isTame;
        }

        public void setTame(boolean tame) {
            isTame = tame;
        }
    }
}