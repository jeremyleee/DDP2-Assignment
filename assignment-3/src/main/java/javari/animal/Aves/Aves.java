package javari.animal.Aves;
import javari.animal.Animal;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;

public class Aves extends Animal {
    private boolean isLayingEgg;

    public Aves(int id, String name, String gender, double length,
                double weight, String egg, Condition condition) {
        super(id, name, gender, length, weight);
        if (egg.equals("laying")) {
            this.isLayingEgg == true;
        }
        else{
            this.isLayingEgg==false;
        }
    }
    public boolean spesificCondition(){
        return !(islayingEgg);
    }
    public boolean isLayingEgg() {
        return isLayingEgg;
    }

    public void setLayingEgg(boolean layingEgg) {
        isLayingEgg = layingEgg;
    }
}